package xxx.com.contentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

class AlumnoSqliteHelper extends SQLiteOpenHelper {

    private static String consultaSQL = "CREATE TABLE Alumnos (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            " nombre TEXT, apellidos TEXT, codigo integer )";

    AlumnoSqliteHelper(Context contexto, String nombre, CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(consultaSQL);

        for (int i = 1; i <= 15; i++) {
            String nombre = "Alumno " + i;
            String apellidos = "apellidos " + i;
            String codigo = i + "";
            db.execSQL("INSERT INTO Alumnos (nombre, apellidos, codigo) " +
                    "VALUES ('" + nombre + "', '" + apellidos + "', '" + codigo + "')");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        db.execSQL("DROP TABLE IF EXISTS Alumno");
        db.execSQL(consultaSQL);
    }
}
