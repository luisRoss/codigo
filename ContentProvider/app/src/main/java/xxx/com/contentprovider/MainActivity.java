package xxx.com.contentprovider;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void consultar(View view) {
        String[] campos = new String[]{"_id", "nombre", "apellidos", "codigo"};
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ClientesProvider.CONTENT_URI, campos, null, null, null);

        Log.i("app", "Se empieza a obtener la información");
        if (cursor.moveToFirst())
            do {
                String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String apellidos = cursor.getString(cursor.getColumnIndex("apellidos"));
                int codigo = cursor.getInt(cursor.getColumnIndex("codigo"));

                Log.i("app", "nombre:" + nombre + " - apellidos:" + apellidos + " -codigo:" + codigo);

            } while (cursor.moveToNext());
    }

    public void insertar(View view) {
        ContentValues values = new ContentValues();
        values.put("nombre", "luis");
        values.put("apellidos", "berthet");
        values.put("codigo", 1);

        ContentResolver cr = getContentResolver();
        cr.insert(ClientesProvider.CONTENT_URI, values);
        Log.i("app", "Se insertan los datos");
    }


    public void eliminar(View view) {
        ContentResolver cr = getContentResolver();
        cr.delete(ClientesProvider.CONTENT_URI, "codigo = 1", null);
        Log.i("app", "Se eliminan los datos");
    }
}
