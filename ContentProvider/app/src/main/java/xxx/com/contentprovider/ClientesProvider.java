package xxx.com.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class ClientesProvider extends ContentProvider {

    private static final String uri = "content://com.xxx.contentprovider/alumnos";
    public static final Uri CONTENT_URI = Uri.parse(uri);

    private static final int ALUMNOS = 1;
    private static final int ALUMNOS_ID = 2;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("com.xxx.contentprovider", "alumnos", ALUMNOS);
        uriMatcher.addURI("com.xxx.contentprovider", "alumnos/#", ALUMNOS_ID);
    }

    private AlumnoSqliteHelper alumnosSQLHelper;

    @Override
    public boolean onCreate() {
        alumnosSQLHelper = new AlumnoSqliteHelper(getContext(), "AlumnosDB", null, 3);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if (uriMatcher.match(uri) == ALUMNOS_ID)
            where = "_id=" + uri.getLastPathSegment();

        SQLiteDatabase db = alumnosSQLHelper.getWritableDatabase();
        Cursor cursor = db.query("Alumnos", projection, where, selectionArgs,
                null, null, sortOrder);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = alumnosSQLHelper.getWritableDatabase();
        long regId = db.insert("Alumnos", null, values);
        Uri newUri = ContentUris.withAppendedId(CONTENT_URI, regId);
        return newUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String where = selection;
        if (uriMatcher.match(uri) == ALUMNOS_ID)
            where = "_id=" + uri.getLastPathSegment();

        SQLiteDatabase db = alumnosSQLHelper.getWritableDatabase();
        int cont = db.update("Alumnos", values, where, selectionArgs);
        return cont;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String where = selection;
        if (uriMatcher.match(uri) == ALUMNOS_ID)
            where = "_id=" + uri.getLastPathSegment();

        SQLiteDatabase baseDatos = alumnosSQLHelper.getWritableDatabase();
        int indice = baseDatos.delete("Alumnos", where, selectionArgs);
        return indice;
    }

    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case ALUMNOS:
                return "vnd.android.cursor.dir/vnd.xxx.alumnos";
            case ALUMNOS_ID:
                return "vnd.android.cursor.item/vnd.xxx.alumnos";
            default:
                return null;
        }
    }
}
