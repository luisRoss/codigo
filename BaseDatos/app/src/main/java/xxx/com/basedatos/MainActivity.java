package xxx.com.basedatos;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private SQLiteDatabase baseDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AlumnosHelper alumnosBD = new AlumnosHelper(this, "DBAlumnos", null, 1);
        baseDatos = alumnosBD.getWritableDatabase();
        eliminarBase();
    }

    public void eliminarBase() {
        baseDatos.execSQL("drop table Alumnos");
        baseDatos.execSQL(AlumnosHelper.consultaCreacion);
    }

    public void insertar(View vista) {
        ContentValues nuevoRegistro = new ContentValues();
        nuevoRegistro.put("nombre", "luis");
        nuevoRegistro.put("apellidos", "berthet");
        nuevoRegistro.put("codigo", "1");
        baseDatos.insert("Alumnos", null, nuevoRegistro);
        Log.i("app", "Se ingresa el alumno");
    }

    public void actualizar(View vista) {
        ContentValues valores = new ContentValues();
        valores.put("nombre", "Alejandro");
        baseDatos.update("Alumnos", valores, "codigo=" + 1, null);
        Log.i("app", "Se actualiza el alumno");
    }

    public void eliminar(View vista) {
        baseDatos.delete("Alumnos", "codigo=" + 1, null);
        Log.i("app", "Se elimina el alumno");
    }

    public void obtener(View vista) {
        String[] campos = new String[]{"nombre", "apellidos", "codigo"};
        Cursor cursorApoyo = baseDatos.query("Alumnos", campos, null, null, null, null, null);

        Log.i("app", " Se muestran los registros");
        if (cursorApoyo.moveToFirst())
            do {
                String nombre = cursorApoyo.getString(0);
                String apellido = cursorApoyo.getString(1);
                Log.i("app", " " + nombre + " - " + apellido);

            } while (cursorApoyo.moveToNext());
    }
}
