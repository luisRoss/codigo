package xxx.com.basedatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

class AlumnosHelper extends SQLiteOpenHelper {

    public static String consultaCreacion =
            "CREATE TABLE Alumnos (codigo int, nombre text, apellidos text)";

    AlumnosHelper(Context contexto, String nombre, CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase baseDatos) {
        baseDatos.execSQL(consultaCreacion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase baseDatos, int versionA, int versionN) {
        baseDatos.execSQL("drop table Alumno");
        baseDatos.execSQL(consultaCreacion);
    }
}

