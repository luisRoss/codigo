package xxx.com.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentListado extends Fragment {

    private Alumno[] alumnos = new Alumno[]{
            new Alumno("Luis", "Berthet", 1),
            new Alumno("Alejandro", "Vega", 2)};

    private ListView listadoAlumnos;
    private AlumnoListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listado, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        listadoAlumnos = (ListView) getView().findViewById(R.id.LstListado);
        listadoAlumnos.setAdapter(new AdaptadorAlumnos(this));
        listadoAlumnos.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> list, View view, int pos, long id) {
                if (listener != null) {
                    Alumno alumnoApoyo = (Alumno) listadoAlumnos.getAdapter().getItem(pos);
                    listener.onAlumnoSeleccionado(alumnoApoyo);
                }
            }
        });
    }

    class AdaptadorAlumnos extends ArrayAdapter<Alumno> {

        Activity context;

        AdaptadorAlumnos(Fragment context) {
            super(context.getActivity(), R.layout.alumno_item, alumnos);
            this.context = context.getActivity();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.alumno_item, null);

            TextView nombre = (TextView) view.findViewById(R.id.nombrecompleto);
            nombre.setText(alumnos[position].getNombre() + " " + alumnos[position].getApellidos());

            TextView numero = (TextView) view.findViewById(R.id.id);
            numero.setText(alumnos[position].getCodigo()+ "");

            return view;
        }
    }

    public interface AlumnoListener {
        void onAlumnoSeleccionado(Alumno alumno);
    }

    public void setAlumnoListener(AlumnoListener listener) {
        this.listener = listener;
    }
}
