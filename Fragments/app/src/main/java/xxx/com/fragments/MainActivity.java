package xxx.com.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MainActivity extends FragmentActivity implements FragmentListado.AlumnoListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentListado frgListado = (FragmentListado)
                getSupportFragmentManager().findFragmentById(R.id.FrgListado);

        frgListado.setAlumnoListener(this);
    }

    @Override
    public void onAlumnoSeleccionado(Alumno alumno) {
        boolean seEncuentraDetalleFramework =
                (getSupportFragmentManager().findFragmentById(R.id.FrgDetalle) != null);

        if (seEncuentraDetalleFramework) {
            FragmentDetalle fragment = (FragmentDetalle) getSupportFragmentManager().
                    findFragmentById(R.id.FrgDetalle);
            fragment.mostrarDetalle(alumno.getNombre() + " " + alumno.getApellidos());

        } else {
            Intent intent = new Intent(this, DetalleActivity.class);
            intent.putExtra("texto", alumno.getNombre());
            startActivity(intent);

        }
    }
}
