package xxx.com.fragments;

class Alumno {
    private String nombre;
    private String apellidos;
    private int codigo;

    Alumno(String nombre, String apellidos, int codigo) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.codigo = codigo;
    }

    String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
