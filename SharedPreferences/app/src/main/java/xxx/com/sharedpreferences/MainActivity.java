package xxx.com.sharedpreferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        escribirInformacion();
        obtenerInformacion();
    }


    public void escribirInformacion() {
        SharedPreferences shared = getSharedPreferences("Datos", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("nombre_usuario", "Berthet");
        editor.apply();

        Log.i("app", "Información guardada correctamente");
    }

    public void obtenerInformacion() {
        SharedPreferences shared = getSharedPreferences("Datos", Activity.MODE_PRIVATE);
        String datoObtenido = shared.getString("nombre_usuario", "");
        Log.i("app", "Información obtenida " + datoObtenido);

    }
}
