package xxx.com.serializacion;

import android.os.Parcel;
import android.os.Parcelable;

public class EmpleadoParceable implements Parcelable {

    private String nombre;
    private String apellidos;
    private int id;

    public EmpleadoParceable(String nombre, String apellidos, int id) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nombre);
        dest.writeString(this.apellidos);
        dest.writeInt(this.id);
    }

    public EmpleadoParceable() {
    }

    protected EmpleadoParceable(Parcel in) {
        this.nombre = in.readString();
        this.apellidos = in.readString();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<EmpleadoParceable> CREATOR = new Parcelable.Creator<EmpleadoParceable>() {
        @Override
        public EmpleadoParceable createFromParcel(Parcel source) {
            return new EmpleadoParceable(source);
        }

        @Override
        public EmpleadoParceable[] newArray(int size) {
            return new EmpleadoParceable[size];
        }
    };

    @Override
    public String toString() {
        return "EmpleadoParceable{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", id=" + id +
                '}';
    }
}
