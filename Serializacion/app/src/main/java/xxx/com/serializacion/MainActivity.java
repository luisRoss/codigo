package xxx.com.serializacion;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serializar();
        deserializar();
    }

    public void cambiar(View view) {
        //parceable
        EmpleadoParceable empleado = new EmpleadoParceable("luis", "berthet", 10);
        Intent intent = new Intent(this, ActividadSalida.class);
        intent.putExtra("objeto", empleado);
        startActivity(intent);
    }

    public void serializar() {
        try {
            FileOutputStream fos = getApplication().openFileOutput("archivo", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(new Empleado("Luis", "Enrique", 1));
            os.close();
            fos.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void deserializar() {
        try {
            FileInputStream fis = getApplication().openFileInput("archivo");
            ObjectInputStream is = new ObjectInputStream(fis);
            Empleado simpleClass = (Empleado) is.readObject();
            is.close();
            fis.close();

            System.out.println("Resultado obtenido " + simpleClass);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
