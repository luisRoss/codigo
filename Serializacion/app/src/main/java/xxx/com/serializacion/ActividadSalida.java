package xxx.com.serializacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadSalida extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_salida);

        EmpleadoParceable empleado = (EmpleadoParceable)
                getIntent().getExtras().getParcelable("objeto");

        TextView texto = (TextView) findViewById(R.id.salida);
        texto.setText(empleado.toString());
    }
}
