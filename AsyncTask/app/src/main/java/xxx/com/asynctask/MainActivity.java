package xxx.com.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    private ProgressDialog dialogoProgreso;
    private TareaDialog tareaCarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void cargarProceso(View vista) {
        dialogoProgreso = new ProgressDialog(MainActivity.this);
        dialogoProgreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogoProgreso.setTitle("Progreso");
        dialogoProgreso.setMessage("Realizando acción");
        dialogoProgreso.setCancelable(true);
        dialogoProgreso.setMax(100);

        tareaCarga = new TareaDialog();
        tareaCarga.execute();
    }

    private void realizaProcesoLargo() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }
    }

    private class TareaDialog extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            for (int contador = 1; contador <= 100; contador++) {
                realizaProcesoLargo();
                publishProgress(contador);

                if (isCancelled()) break;

            }

            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            dialogoProgreso.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            dialogoProgreso.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    TareaDialog.this.cancel(true);
                }
            });

            dialogoProgreso.setProgress(0);
            dialogoProgreso.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                dialogoProgreso.dismiss();
                Toast.makeText(MainActivity.this, "Tarea terminada!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MainActivity.this, "Tarea cancelada!", Toast.LENGTH_SHORT).show();
        }
    }
}
