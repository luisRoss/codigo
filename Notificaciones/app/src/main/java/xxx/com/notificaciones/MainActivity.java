package xxx.com.notificaciones;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void crear_notificacion(View vista) {
        BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_launcher);

        NotificationCompat.Builder notificacionBuilder = new NotificationCompat.Builder(MainActivity.this);
        notificacionBuilder.setLargeIcon(bitmap.getBitmap());
        notificacionBuilder.setSmallIcon(android.R.drawable.stat_sys_warning);
        notificacionBuilder.setContentTitle("Mensaje de notificación");
        notificacionBuilder.setContentText("Ejemplo de mensaje de alerta de notificación");
        notificacionBuilder.setContentInfo("4");
        notificacionBuilder.setTicker("Notificación!!!!!");

        //Se define el intent
        Intent notIntent = new Intent(MainActivity.this, MainActivity.class);
        PendingIntent contIntent = PendingIntent.getActivity(MainActivity.this, 0, notIntent, 0);

        notificacionBuilder.setContentIntent(contIntent);

        //Se lanza la notificación
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, notificacionBuilder.build());
    }
}
