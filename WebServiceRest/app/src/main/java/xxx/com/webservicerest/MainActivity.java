package xxx.com.webservicerest;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnInsertar = (Button) findViewById(R.id.btnInsertar);
        Button btnActualizar = (Button) findViewById(R.id.btnActualizar);
        Button btnEliminar = (Button) findViewById(R.id.btnEliminar);
        Button btnObtener = (Button) findViewById(R.id.btnObtener);
        Button btnListar = (Button) findViewById(R.id.btnListar);

        btnInsertar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new TareaInsertar().execute();
            }
        });

        btnActualizar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new TareaActualizar().execute();
            }
        });

        btnEliminar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new TareaEliminar().execute();
            }
        });

        btnListar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TareaObtenerTodos().execute();
            }
        });

        btnObtener.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TareaObtenerUno().execute();
            }
        });
    }

    private class TareaObtenerTodos extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {

            HttpClient httpClient = new DefaultHttpClient();
            HttpGet del = new HttpGet("http://jsonplaceholder.typicode.com/users");
            del.setHeader("content-type", "application/json");

            try {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                JSONArray respJSON = new JSONArray(respStr);
                Log.i("app", "tamaño obtenido " + respJSON.length());

                for (int i = 0; i < respJSON.length(); i++) {
                    JSONObject obj = respJSON.getJSONObject(i);

                    int idCli = obj.getInt("id");
                    String nombCli = obj.getString("name");
                    String telefCli = obj.getString("phone");
                    Log.i("app", idCli + "-" + nombCli + "-" + telefCli);
                }

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);
            }

            return true;
        }
    }

    private class TareaObtenerUno extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient();

            String id = "1";
            HttpGet del = new HttpGet("http://jsonplaceholder.typicode.com/users/" + id);
            del.setHeader("content-type", "application/json");

            try {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());
                JSONObject respJSON = new JSONObject(respStr);

                int idCli = respJSON.getInt("id");
                String nombCli = respJSON.getString("name");
                String telefCli = respJSON.getString("phone");

                Log.i("app", idCli + " - " + nombCli + " - " + telefCli);

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);

            }

            return true;
        }
    }

    private class TareaInsertar extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://jsonplaceholder.typicode.com/users");
            post.setHeader("content-type", "application/json");

            try {

                Log.i("app", "Se inserta");
                JSONObject dato = new JSONObject();

                dato.put("name", "luis");
                dato.put("phone", "58-54-27-02");

                StringEntity cadenaEntidad = new StringEntity(dato.toString());
                post.setEntity(cadenaEntidad);

                HttpResponse resp = httpClient.execute(post);
                String respStr = EntityUtils.toString(resp.getEntity());

                Log.i("app", respStr);
                Log.i("app", "insertado correctamente");

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {

        }
    }

    private class TareaActualizar extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {

            HttpClient httpClient = new DefaultHttpClient();
            String id = "1";
            HttpPut put = new HttpPut("http://jsonplaceholder.typicode.com/users/" + id);
            put.setHeader("content-type", "application/json");

            try {
                JSONObject dato = new JSONObject();

                dato.put("id", 2);
                dato.put("name", "luis");
                dato.put("phone", "58-54-27-02");

                StringEntity entity = new StringEntity(dato.toString());
                put.setEntity(entity);

                HttpResponse resp = httpClient.execute(put);
                String respStr = EntityUtils.toString(resp.getEntity());

                Log.i("app", respStr);

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);

            }

            return true;
        }

        protected void onPostExecute(Boolean result) {

        }
    }

    private class TareaEliminar extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient();
            String id = "1";

            HttpDelete del = new HttpDelete("http://jsonplaceholder.typicode.com/users/" + id);
            del.setHeader("content-type", "application/json");

            try {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                Log.i("app", respStr);
                Log.i("app", "Borrado correctamente");

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);

            }

            return true;
        }

        protected void onPostExecute(Boolean result) {
        }
    }

}
